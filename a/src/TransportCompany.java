import java.util.ArrayList;
import java.util.Scanner;

public class TransportCompany {
    public static void main(String[] args) throws IllegalAccessException {
        Scanner in = new Scanner(System.in);
        ArrayList<String> Vehicles = new ArrayList<>();
        ArrayList<ArrayList<String>> Trucks = new ArrayList<>();
        ArrayList<ArrayList<String>> Vans = new ArrayList<>();
        ArrayList<ArrayList<String>> Cars = new ArrayList<>();
        System.out.println("This is a Vehicles Creator Software of 'Zephyr Motors Company'");
        while (true){
            System.out.println("Select: ");
            System.out.println("1. Add Vehicle");
            System.out.println("2. Check Vehicles List ");
            System.out.println("3. Exit the program");
            int Selection = 0;
            while (true){
                try {
                    Selection = in.nextInt();
                    break;
                }catch (Exception e){
                    System.out.print("Try again with a valid option : ");
                    in.nextLine();  //clean the buffer
                }
            }
            switch (Selection){
                case 1:
                    int Option = 0;
                    System.out.println("Please choose an option: ");
                    System.out.println("1. Truck");
                    System.out.println("2. Van");
                    System.out.println("3. Car");
                    while (true) {
                        try {
                            Option = in.nextInt();
                            break;
                        } catch (Exception e) {
                            System.out.print("Try again with a valid option : ");
                            in.nextLine();  //clean the buffer
                        }
                    }
                    switch (Option){
                        case 1:
                            Truck a = new Truck();
                            Trucks.add(a.TruckSet());


                            break;

                        case 2:
                            Van b = new Van();
                            Vans.add(b.VanSet());


                            break;

                        case 3:
                            Car c = new Car();
                            Cars.add(c.CarSet());
                            break;

                        default:
                            System.out.println("Try again with a valid option.");
                            break;
                    }
                    break;
                case 2:
                    int TrucksMount = 0;
                    System.out.print("Trucks: ");
                    for (int i = 0; i < Trucks.size(); i++) {
                        TrucksMount += 1;
                    }
                    System.out.println(TrucksMount);
                    System.out.println(Trucks);

                    int VansMount = 0;
                    System.out.print("Vans: ");
                    for (int i = 0; i < Vans.size(); i++) {
                        VansMount += 1;
                    }
                    System.out.println(VansMount);
                    System.out.println(Vans);

                    int CarsMount = 0;
                    System.out.print("Cars: ");
                    for (int i = 0; i < Cars.size(); i++) {
                        CarsMount += 1;
                    }
                    System.out.println(CarsMount);
                    System.out.println(Cars);
                    break;

                default:
                    System.out.println("Thanks by use 'Zephyr Motors Company', we expect you've got a great experience.");
                    System.out.println("Closing...");
                    System.exit(0);

            }
        }
    }
}